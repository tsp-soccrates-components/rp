let consts = {
  RP_API_ADDRESS: "https://rp-backend.soccrates.xyz", // Can be overide by config.js
  URL_GENERATE_PLAYBOOK: "generatePlaybook",
  URL_GET_COA_RESULTS: "getCoAResults",
  INDEX_DEFENSES_SUBTABLE: 8,
  PRECISION_NUMBER: 4,
};
let aiv = 0;
let ale = 0;
let initialTTC = 0;
let coas = [];
let containment = "";
let table = "";


function load_request_coas(config) {
  console.log(config);
  if (!config) {
      config = {};
  }
  let default_config = consts;
  for (let key in default_config) {
      if (default_config.hasOwnProperty(key) && !config.hasOwnProperty(key)) {
          config[key] = default_config[key];
      }
  }
  consts = config;
  const queryString = window.location.search;
  var urlParams = new URLSearchParams(queryString);
  let request_timestamp = "0";
  if (urlParams.has('request')){
    request_timestamp = urlParams.get('request');
  }
  loadData(request_timestamp);
};


function loadData(request_timestamp) {
  console.log("Send request to the RP API to load results");
  let data_to_send = "";
  let urlArgs = `request_timestamp=${request_timestamp}`;
  let address = consts.RP_API_ADDRESS;
  let service = consts.URL_GET_COA_RESULTS;
  let service_url = `${address}/${service}?${urlArgs}`;
  console.log(service_url);
  $.ajax({
    type: 'post',
    url: service_url,
    async: true,
    dataType: "json",
    contentType: "application/json",
    data: data_to_send,
    success: function (response) {
      let inputdata = response
      console.log('Response from api');
      console.log(inputdata);
      initGUI(inputdata)
    },
    error : function(xhr, textStatus, errorThrown ) {
      console.log("Error on loading results data");
      console.log(textStatus);
      console.log(errorThrown);
      let display_message = "<h2>Error on loading results data from the Response Planner API</h2>";
      display_message += "<p>" + textStatus + "</p>";
      document.write(display_message)
    }
  });
};


function initGUI(inputdata) {
  aiv = String(money_formatter.format(inputdata["aiv"]));
  ale = String(money_formatter.format(inputdata["ale"]));
  containment = inputdata["containment"];
  coas = inputdata["coas"];
  document.getElementById("aiv_input").textContent = aiv;
  document.getElementById("ale_input").textContent = ale;
  displayContainemnt(containment);
  setupTable();
}


let displayContainemnt = function(containment){
  let text_to_display = '';
  if (containment){
    text_to_display = 'Containment CoAs';
  }
  else{
    text_to_display = 'CoAs';
  }
  text_to_display += ' Table';
  document.getElementById("table_title").textContent = text_to_display;
}


var printRORI = function(cell, formatterParams){
  let value = cell.getValue();
  if (value){
    color_font = 'black'
    if (value < 0){
      color_font = 'red'
    }
    value = String(value.toPrecision(consts.PRECISION_NUMBER));
    return  "<span style='color:" + color_font +  ";font-weight:bold;'>" + value + "</span>";
  }
  return value;
  };


var printBIA = function(cell, formatterParams){
  let value = cell.getValue();
  if (value){
    value = String(value.toPrecision(consts.PRECISION_NUMBER));
    color_font = "green"
    if (value > 2/3){
      color_font = 'red'
    }
    else if (value > 1/3) {
      color_font = '#dba226'
    }
    return "<span style='color:" + color_font +  ";font-weight:bold;'>" + value + "</span>";
  }
  return value;
};


function addDefensesHosts(defenses_hosts){
  let html_host_to_add;
  let html_defenses_hosts = "";
  for (let i = 0; i < defenses_hosts.length; i++){
    html_host_to_add = "<span class='hostDisplay'>" +  defenses_hosts[i] + ", </span>";
    html_defenses_hosts += html_host_to_add;
  }
  return html_defenses_hosts;
};


function setupTable() {
  table = new Tabulator("#coas-table", {
   	data: coas,
    //layout:"fitDataStretch", //fit columns to width of table (optional)    responsiveLayout:"collapse",
    layout:"fitColumns",
    responsiveLayout:"collapse",
    selectable:true,
    columnDefaults:{resizable:true,},
   	columns:[ //Define Table Columns
      {formatter:"responsiveCollapse", width:30, minWidth:30, hozAlign:"center", resizable:false, headerSort:false},
      // {width:30, minWidth:30, hozAlign:"center", resizable:false, headerSort:false},
  	 	{title:"Name", field:"name", width:150},
  	 	{title:"RORI", field:"RORI", hozAlign:"left", formatter:printRORI, width:100},
  	 	{title:"Business Impact", field:"business_impact", formatter:printBIA},
      {title:"Containment", field:"containment"},
      {title:"Monetary Cost", field:"monetary_cost", formatter:"money"},
     ],
    rowFormatter:function(row){
      //create and style holder elements
      var holderEl = document.createElement("div");
      var tableEl = document.createElement("div");
      var tableDefensesHosts = document.createElement("div");

      holderEl.classList.add('tohide');
      holderEl.style.boxSizing = "border-box";
      holderEl.style.padding = "10px 30px 10px 10px";
      holderEl.style.borderTop = "1px solid #333";
      holderEl.style.borderBotom = "1px solid #333";
      holderEl.style.background = tableEl.style.background;
      // Hide CoA details at first
      holderEl.style.display = "none";
      tableEl.style.border = "1px solid #333";
      tableDefensesHosts.style.border = "1px solid #333";


      var subTable = new Tabulator(tableEl, {
         layout:"fitColumns",
         // layout:"fitDataStretch",
         data:row.getData().defenses,
         columns:[
         {title:"Target", field:"ref", width:150, resizable:false},
         {title:"Defense Name", field:"defenseName", width:200},
         {title:"Defense Info", field:"defenseInfo"},
         {title:"Mitre Ref", field:"mitreRef",  width:120, resizable:false}
         ]
      });
      tableDefensesHosts.innerHTML = "<span class='hostsDisplay'> Defenses hosts : " + addDefensesHosts(row.getData().defenses_hosts) + "</span>";

      holderEl.appendChild(tableEl);
      holderEl.appendChild(tableDefensesHosts);
      row.getElement().appendChild(holderEl);
    }
  });};

//select row on "select all" button click
document.getElementById("select-all").addEventListener("click", function(){
  table.selectRow();
});

//deselect row on "deselect all" button click
document.getElementById("deselect-all").addEventListener("click", function(){
  table.deselectRow();
});

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


function toggleDisplay(){
  var toggleButton = document.getElementById("toggleButton");
  let displayStatus = '';
  if (toggleButton.value == "Hide Details"){
    toggleButton.value = "Show Details";
    displayStatus = 'none';
  } else {
    toggleButton.value = "Hide Details";
    displayStatus = 'block';
  }
  let tohide_els = document.getElementsByClassName("tohide");
  for(var i = 0; i < tohide_els.length; i++){
    tohide_els[i].style.display = displayStatus;
  }
  table.redraw();
  sleep(22).then(() => {
    table.redraw();
  });
}


function getPlaybook_selectedRows() {
  console.log("getPlaybook click");
  var selectedRows = table.getSelectedRows();
  console.log(selectedRows);
  for (let row of selectedRows){
    var coa_to_transform = row.getData();
    getPlaybook(coa_to_transform);
  }
}


function getPlaybook(coa) {
  var data_to_send = window.JSON.stringify(coa);
  console.log("Send Impact computation request to the Python API");
  console.log(data_to_send);
  let address = consts.RP_API_ADDRESS;
  let service = consts.URL_GENERATE_PLAYBOOK;
  let service_url = `${address}/${service}`;
  $.ajax({
      type: 'post',
      url: service_url,
      async: true,
      dataType: "json",
      contentType: "application/json",
      data: data_to_send,
      success: function (response) {
      }
  }).done(function (response_data) {
      console.log("Ajax response");
      console.log(response_data);
      // Open playbook on a new tab
      var playbook =  JSON.stringify(response_data,null,2);
      var newTab = window.open("data:text/json," + encodeURIComponent(playbook), "_blank");
  });
}

const money_formatter = new Intl.NumberFormat('de', {
  style: 'currency',
  currency: 'EUR',
  maximumFractionDigits: 0,
  roundingIncrement: 5
})
