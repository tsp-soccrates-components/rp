from enum import Enum, unique

@unique
class DefenseName(Enum):
    HOST_ISOLATION = 'Host isolation'
    TRAFFIC_FILTERING = 'Traffic filtering'
    NO_DEFENSE = ''


DEFENSE_INFO = {
    DefenseName.HOST_ISOLATION: 'Isolate the host from the network by using Network Segmentation',
    DefenseName.TRAFFIC_FILTERING: 'Use network appliances to filter ingress or egress traffic and perform protocol-based filtering',
    DefenseName.NO_DEFENSE: 'No defense provided'
}


MITRE_REF = {
    DefenseName.HOST_ISOLATION: 'M1030',
    DefenseName.TRAFFIC_FILTERING: 'M1037',
    DefenseName.NO_DEFENSE: ''
}


class ContainmentDefense():
    def __init__(self, defenseName_str, target):
        self.defenseName = self.get_defense_name(defenseName_str)
        self.defenseInfo = DEFENSE_INFO[self.defenseName]
        self.mitreRef = MITRE_REF[self.defenseName]
        self.target = target

    def get_defense_name(self, defenseName_str):
        try:
            return DefenseName(defenseName_str)
        except ValueError as e:
            print(e)
            return DefenseName.NO_DEFENSE

    @classmethod
    def is_host_isolation(cls, defenseName_str):
        return defenseName_str == DefenseName.HOST_ISOLATION.value

    @classmethod
    def is_traffic_filtering(cls, defenseName_str):
        return defenseName_str == DefenseName.TRAFFIC_FILTERING.value

    def asdict(self):
        targetLabel = 'target'
        if self.is_host_isolation(self.defenseName.value):
            targetLabel = 'hosts'
        elif self.is_traffic_filtering(self.defenseName.value):
            targetLabel = 'flows'
        defense = {
            'defenseName':self.defenseName,
            'defenseInfo':self.defenseInfo,
            targetLabel:self.target,
            'mitreRef':self.mitreRef
            }
        return defense
