#!/usr/bin/env python3
from pydantic import BaseModel, Field
from typing import List, Dict


# Small Classes
class Aiv:
    DEFAULT = 123456
    STR = 'Annual Infrastructure Value'


class Ale:
    DEFAULT = 1234
    STR = 'Annual Loss Expectancy'


class Containment:
    DEFAULT = False
    STR = 'Is a Containment Course of Actions'


class Monetary_Cost:
    DEFAULT = {"1":123,"2":567,"3":789,"4":123,"5":456}
    STR = 'Monetary Cost of the Course of Action'


class Defenses:
    STR = 'List of defenses'


class DefenseInfo:
    DEFAULT = 'No Information'
    STR = "Description of the defense"


class DefenseName:
    DEFAULT = 'Patch'
    STR = 'Name of a defense in securiCAD'


class MitreRef:
    DEFAULT = 'M0000'
    STR = "Reference to a mitigation in mitre attack"


class Ref:
    DEFAULT = 112233
    STR = "Reference of the Coa to an asset ID in the IMC"


class Rori:
    DEFAULT = None
    STR = 'Return on Response Investment Index of the CoA'


class SiemEvent:
    DEFAULT = "TA0001"
    STR = "Siem event"


class TTC_Dict:
    DEFAULT = {"Default_asset":[1,1,1], "Default_asset_2":[1,1,1]}
    STR = 'Time to compromised values (ttc5, ttc50 and ttc95) for targeted assets'

class SecuriCAD_ids_Dict:
    DEFAULT = {"pid": '446664605265379', "tid": '975447083382514', 'simid':'137151311040516'}
    STR = 'SecuriCAD simulation ids from the ADG component'

# Big Classes
class Flow(BaseModel):
    sourceIP: str = Field(default = "192.168.0.3", titre = "Source IP address of the flow")
    destinationIP: str = Field(default = "123.45.67.89", titre = "Destination IP address of the flow")


class Defense(BaseModel):
    ref: int = Field(default = Ref.DEFAULT, title = Ref.STR)
    defenseName: str = Field(default = DefenseName.DEFAULT, title = DefenseName.STR)
    defenseInfo: str = Field(default = DefenseInfo.DEFAULT, title = DefenseInfo.STR)
    mitreRef: str = Field(default = MitreRef.DEFAULT, title = MitreRef.STR)
    hosts: List[str] = Field(default = None, titre = "List of Hosts targeted by the Coa")
    flows: List[Flow] = Field(default = None, titre = "List of Flows targeted by the Coa")


class Coa(BaseModel):
    RORI: float = Field(default = Rori.DEFAULT, titre = Rori.STR)
    containment: bool = Field(default = Containment.DEFAULT, titre = Containment.STR)
    coaTTC: Dict[str, List[float]] = Field(default = TTC_Dict.DEFAULT, titre = f'CoA {TTC_Dict.STR}')
    coa_ids: Dict = Field(default = SecuriCAD_ids_Dict.DEFAULT, titre = f'CoA {SecuriCAD_ids_Dict.STR}')
    monetary_cost: Dict[str, float] = Field(default = Monetary_Cost.DEFAULT, titre = Monetary_Cost.STR)
    defenses: List[Dict] = Field(default = None, title = Defenses.STR)


class Incident_Info(BaseModel):
    sourceIPs: List[str] = Field(default = ["192.168.0.3"],
                                 title = "List of source IP addresses")
    destinationIPs: List[str] = Field(default = ["123.45.67.89"],
                                      title = "List of destination IP addresses")


# Examples
SIEM_EVENT_EXAMPLE = "TA0001" # Initial Access
INITIAL_TTC_EXAMPLE = TTC_Dict.DEFAULT
INITIAL_IDS_EXAMPLE = SecuriCAD_ids_Dict.DEFAULT
CONTAINMENT_EXAMPLE = False # Bool
DEFENSE_EXAMPLE = {
    "ref": "112233",
    "defenseName": "Patched",
    "defenseInfo": "Patch application",
    "mitreRef": "M1040"
    }
DEFENSE_EXAMPLE_2 = {
    "ref": "112244",
   "defenseName": "Isolate",
   "defenseInfo": "Network Segmentation",
   "mitreRef": "M1030",
   "hosts": ["192.168.0.3", "192.168.0.10"]
    }
DEFENSE_EXAMPLE_3 = {
    "ref": "112255",
    "defenseName": "Filter",
    "defenseInfo": "Filter Network Traffic",
    "mitreRef": "M1037",
    "flows": [{"src": "192.168.0.3", "dst": "123.45.67.89"}]
    }

COA_EXAMPLE = {
    "containment": False,
    "coaTTC": {"Default_asset":[89,126,534],"Default_asset_2":[323,430,540]},
    "coa_ids": {"pid": "446664605265379", "tid":"975447083382514", "simid":"252411587630008"},
    "monetary_cost": Monetary_Cost.DEFAULT,
    "defenses": [DEFENSE_EXAMPLE]
    }
COA_EXAMPLE_2 = {
    "containment": False,
    "coaTTC": {"Default_asset":[189,826,934],"Default_asset_2":[85,230,340]},
    "coa_ids": {"pid": "446664605265379", "tid":"975447083382514", "simid":"330443144764449"},
    "monetary_cost": {"1":144,"2":878,"3":455,"4":1423,"5":6},
    "defenses": [DEFENSE_EXAMPLE_2, DEFENSE_EXAMPLE_3]
    }
COAS_EXAMPLE = [COA_EXAMPLE, COA_EXAMPLE_2]
CONTAINMENT_COA_EXAMPLE_1 = {
    "defenses": [DEFENSE_EXAMPLE_2]
    }
CONTAINMENT_COA_EXAMPLE_2 = {
    "defenses": [DEFENSE_EXAMPLE_2, DEFENSE_EXAMPLE_3]
    }
CONTAINMENT_COAS_EXAMPLE = [CONTAINMENT_COA_EXAMPLE_1, CONTAINMENT_COA_EXAMPLE_2]
RANKING_COAS_INPUT_EXAMPLE = {"siem_event": SIEM_EVENT_EXAMPLE,
                              "initialTTC": INITIAL_TTC_EXAMPLE,
                              "initial_ids": INITIAL_IDS_EXAMPLE,
                              "coas": COAS_EXAMPLE}
SOURCE_IPS_EXAMPLE = ["192.168.0.3", "192.168.0.10"]
DESTINATION_IPS_EXAMPLE = ["123.45.67.89", "123.98.76.54"]
INCIDENT_INFO_EXAMPLE = {"sourceIPs": SOURCE_IPS_EXAMPLE,
                         "destinationIPs": DESTINATION_IPS_EXAMPLE}
PLAYBOOK_EXAMPLE_0 = {"Apply Defenses"}
PLAYBOOK_EXAMPLE_1 = {
    "type": "playbook",
    "spec_version": "1.0",
    "id": "playbook--soccrates-coa-123456123456",
    "name": "Soccrates CoA Playbook",
    "description": "This playbook will apply the CoA in the system",
    "playbook_types": ["prevention"],
    "created_by": "identity--uuid",
    "created": "2021-07-19T23:32:24.399Z",
    "labels": ["Patch", "2FA", "CoA"],
    "features": {"parallel_processing": True},
    "workflow_start": "start--soccrates-coa-123456123456",
    "workflow":
        {
         "start--soccrates-coa-123456123456": {
          "type": "start",
          "on_completion": "parallel--soccrates-coa-123456123456"
         },
        "parallel-soccrates-coa-123456123456": {
          "type": "parallel",
          "name": "Apply defenses of the Coa",
          "next_steps": [
            "defense1--soccrates-coa-123456123456",
            "defense2--soccrates-coa-123456123456"
          ]
        },
        "defense1--soccrates-coa-123456123456": {
          "type": "single",
          "name": "Patch Application",
          "description": "This step will Patch the application on selected hosts",
          "on_completion": "end--soccrates-coa-123456123456",
          "target": "112233",
          "commands": [
            {
            "type": "manual",
            "command": "Download and install the patch for the application in selected hosts"
            }
          ]
        },
        "defense2--soccrates-coa-123456123456": {
          "type": "single",
          "name": "Two Factor Authentication",
          "description": "This step will enable Two Factor Authentication",
          "on_completion": "end--soccrates-coa-123456123456",
          "target": "445566",
          "commands": [
           {
            "type": "manual",
            "command": "Enable Two Factor Authentication on the selected hosts"
           }
          ]
        },
        "end--soccrates-coa-123456123456": {
        "type": "end"
        }
       }
    }
RP_GUI_COA_EXEMPLE = {
    'id': 1,
    'name': 'CoA 1',
    'containment': True,
    'rori': '4.722e-10',
    'bia': 0.31,
    'coa_ttc': 1200,
    'cost': 4555,
    'defenses': [{'ref': 112233,
                 'defenseName': 'Patched',
                 'defenseInfo': 'Patch application',
                 'mitreRef': 'M1040'},
                 {'ref': 112233,
                 'defenseName': 'Update',
                 'defenseInfo': 'Update application',
                 'mitreRef': 'M1040'}],
    'hosts': ['123.89.45.22', '256.0.0.25', '88.142.111.020', '123.89.45.22', '256.0.0.25', '88.142.111.020', '123.89.45.22', '256.0.0.25', '88.142.111.020', '123.89.45.22', '256.0.0.25', '88.142.111.020']
}
