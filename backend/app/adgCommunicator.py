from securicad import enterprise
from securicad.enterprise.exceptions import StatusCodeException
import os

class AdgCommunicatorError(ValueError):
    pass


class AdgCommunicator():
    def __init__(self):
        self.set_config()
        self.client_adg = self.init_client_adg()
    def set_config(self):
        self.base_url = os.environ.get("SECURICAD_URL")
        self.username = os.environ.get("SECURICAD_USERNAME")
        self.password = os.environ.get("SECURICAD_PASSWORD")
        self.organization = os.environ.get("SECURICAD_ORG")
        self.cacert = False
        if os.environ.get("SECURICAD_CACERT"):
            # should only be set if non-empty (not "" or None)
            self.cacert = os.environ.get("SECURICAD_CACERT")
    def init_client_adg(self):
        try:
            print("Log in to Enterprise...", end="")
            client_adg = enterprise.client(
                base_url=self.base_url,
                username=self.username,
                password=self.password,
                organization=self.organization,
                cacert=self.cacert if self.cacert else False
            )
            print("done")
        except StatusCodeException as e:
            raise AdgCommunicatorError("Error when initializing connection to "
                                       "the ADG, please check the Securicad "
                                       "credentials in config and the connection "
                                       "between RP and ADG") from e
        except ValueError as e:
            raise AdgCommunicatorError("Error when initializing connection to "
                                       "the ADG, please check the Securicad "
                                       "credentials in /config/coa-gen.env, "
                                       "SECURICAD_URL, SECURICAD_USERNAME, "
                                       "SECURICAD_PASSWORD, SECURICAD_ORG are "
                                       "needed") from e
        return client_adg

    def get_sim_result(self, pid, tid, simid):
        try:
            project_adg = self.client_adg.projects.get_project_by_pid(pid)
            scenario_adg = project_adg.get_scenario_by_tid(tid)
            simulation = scenario_adg.get_simulation_by_simid(simid)
            sim_result = simulation.get_results()
        except StatusCodeException as e:
            raise AdgCommunicatorError("Error when retrieving simulation results "
                                       "from ADG, please check that the set of "
                                       f"ids (pid:{pid}, tid:{tid}, simid:{simid}) "
                                       "match a simulation previously "
                                       "executed in the ADG") from e
        return sim_result['results']

    def get_all_simulations_ids(self):
        all_simulations_ids = []
        for p in self.client_adg.projects.list_projects():
            project_dict = {'project_name':p.name}
            try:
                for s in p.list_scenarios():
                    scenario_dict = dict(project_dict)
                    scenario_dict["scenario_name"] = s.name
                    try:
                        for sim in s.list_simulations():
                            sim_dict = dict(scenario_dict)
                            sim_dict["sim_name"] = sim.name
                            sim_dict["pid"] = sim.pid
                            sim_dict["tid"] = sim.tid
                            sim_dict["simid"] = sim.simid
                            try:
                                res = sim.get_results()['results']
                                sim_dict['maxrisk'] = res['maxrisk']
                                sim_dict['risk'] = res['risk']
                            except Exception as e:
                                print(f'Error with Simulation {sim.name}, pid:'
                                      f'{sim.pid}, tid:{sim.tid}, simid:{sim.simid}')
                                print(e)
                            all_simulations_ids.append(sim_dict)
                    except Exception as e:
                        print(f"Error with Scenario {s.name},  pid:{s.pid}, tid:{s.tid}")
                        print(e)
            except Exception as e:
                print(f"Error with Project {p.name}, pid:{p.pid}")
                print(e)
        return all_simulations_ids
