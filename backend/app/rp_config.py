"""Configuration file for the Response Planner API."""
from os import environ

# RP config
HOST_IP_OR_DOMAIN = environ.get("HOST_IP_OR_DOMAIN")
RP_GUI_ADDRESS = f'https://rp.{HOST_IP_OR_DOMAIN}' #If empty, take the request client address by default

# BIA Config
# BIA_BACKEND_SERVICE = 'http://bia-backend'
BIA_BACKEND_SERVICE = environ.get("BIA_BACKEND_SERVICE")


# Operator email where to send defenses from playbooks to be manually deployed.
OPERATOR_EMAIL = "human.operator@default.email"

# RORI parameters config
## Annual Infrastructure Value
ANNUAL_INFRASTRUCTURE_VALUE = 61000
## Annual Attack Rate (Annual number of attacks the company faces)
ANNUAL_ATTACK_RATE = 1

SAVING_DIRECTORY = "app/savedRequests"
PREFIX_FILENAME = f'{SAVING_DIRECTORY}/rp_coaRanking_'
SUFFIX_FILENAME = ".log"
