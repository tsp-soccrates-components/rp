from enum import Enum, unique

@unique
class Label(Enum):
    INITIAL_ACCESS = "TA0001"
    LATERAL_MOVEMENT = "TA0008"
    DATA_EXFILTRATION = "TA0010"
    NO_LABEL = "NO_LABEL"


EXPOSURE_FACTOR = { 
    Label.INITIAL_ACCESS: 0.1,
    Label.LATERAL_MOVEMENT: 0.4,
    Label.DATA_EXFILTRATION: 0.8,
    Label.NO_LABEL: 0.3
}


class SiemLabel():
    def __init__(self, label_str):
        self.label = self.readLabel(label_str)
        self.exposure_factor = EXPOSURE_FACTOR[self.label]

    def readLabel(self, label_str):
        try:
            label = Label(label_str)
        except ValueError as e:
            print(e)
            label = Label.NO_LABEL
        return label

    def get_exposure_factor(self):
        return self.exposure_factor

    def isInitialAccess(self):
        return self.label == Label.INITIAL_ACCESS

    def isLateralMovement(self):
        return self.label == Label.LATERAL_MOVEMENT

    def isDataExfiltration(self):
        return self.label == Label.DATA_EXFILTRATION
