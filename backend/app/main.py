#!/usr/bin/env python3
import uvicorn

from fastapi import FastAPI, Request, Response, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.staticfiles import StaticFiles
import json
from pydantic import BaseSettings
from typing import Dict, List

from app.adgCommunicator import AdgCommunicatorError, AdgCommunicator
from app.coa_ranking import CoasEvaluator
import app.containment_CoA as containment_CoA
from app.playbook import Playbook
import app.rp_const as rp_const
import app.rp_config as rp_config


class Settings(BaseSettings):
    aar: float = rp_config.ANNUAL_ATTACK_RATE
    aiv: float = rp_config.ANNUAL_INFRASTRUCTURE_VALUE

# Initialization of the api
origins = [
    rp_config.RP_GUI_ADDRESS,
    "https://rp.dev.soccrates.xyz"
]
settings = Settings()
app_title = "Response Planner API"
app_version = '1.6.3'
app = FastAPI(title=app_title, version=app_version, docs_url=None)
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.mount("/static", StaticFiles(directory="app/static"), name="static")

#Default response
@app.get("/")
def get_home_message():
    return {"message": "This is the Response Planner API"}


@app.get("/docs", include_in_schema=False)
async def get_docs():
    return get_swagger_ui_html(
        openapi_url=app.openapi_url,
        title=app.title + " - Swagger UI",
        swagger_js_url="/static/swagger-ui-bundle.js",
        swagger_css_url="/static/swagger-ui.css",
        swagger_favicon_url="/static/swagger_api_icon.png",
    )


@app.get("/get_aiv")
def get_aiv():
    return {"Annual Infrastructure Value": settings.aiv}


@app.post("/update_aiv", status_code=200)
def update_aiv(new_aiv: float = settings.aiv):
    settings.aiv = new_aiv
    return {"message": "Annual Infrastructure Value has been updated"}


@app.get("/get_aar")
def get_aar():
    return {"Annual Attack Rate": settings.aar}


@app.post("/update_aar", status_code=200)
def update_aar(new_aar: float = settings.aar):
    settings.aar = new_aar
    return {"message": "The estimation of the Annual Attack Rate has been updated"}


@app.post("/coa_ranking", status_code=200)
def rankCoa(response: Response,
            siem_label: str = "",
            initial_ids: Dict = rp_const.INITIAL_IDS_EXAMPLE,
            containment: bool = False,
            coas: List[rp_const.Coa] = rp_const.COAS_EXAMPLE):
    """ Rank classic CoAs by RORI, and Containment CoAs by BIA, then generate a
    CACAO Playbook for each CoA. """
    aiv = settings.aiv
    aar = settings.aar
    try:
        coas_evalutor = CoasEvaluator(containment=containment,
                                      siem_label=siem_label,
                                      aiv=aiv,
                                      aar=aar,
                                      initial_ids=initial_ids)
        evaluation = coas_evalutor.eval(coas=coas)
        print(f'CoA ranking: {evaluation}')
    except AdgCommunicatorError as e:
        response.status_code = status.HTTP_403_FORBIDDEN
        error_message = {"error":e.__repr__(), "cause":e.__cause__}
        print(error_message)
        return error_message
    except ValueError as e:
        response.status_code = status.HTTP_403_FORBIDDEN
        error_message = {"error":e.__repr__(), "context":e.__context__}
        print(error_message)
        return error_message
    return evaluation


@app.post("/generateContainmentCoas", status_code=200)
def generateContainmentCoas(response: Response,
                            type: str = "containment",
                            sourceIPs: List[str] = rp_const.SOURCE_IPS_EXAMPLE,
                            destinationIPs: List[str] = rp_const.DESTINATION_IPS_EXAMPLE,
                            siem_label: str = ""):
    try:
        result = containment_CoA.gen_containment_coas(type=type,
                                                      sourceIPs=sourceIPs,
                                                      destinationIPs=destinationIPs,
                                                      siem_label_str=siem_label)
    except ValueError as e:
        response.status_code = status.HTTP_400_BAD_REQUEST
        explanation = "Type argument must be 'containment' or 'stop_exfiltration'"
        error_message = {"error":e.__str__(), "explanation": explanation,
                         "context":e.__context__}
        print("ValueError raised by wrong Containment Generation Type")
        print(error_message)
        return error_message
    return result


@app.post("/generatePlaybook", status_code=200)
def generatePlaybook(coa: dict = rp_const.RP_GUI_COA_EXEMPLE):
    """ Generate a playbook with manual or OpenC2 commands from a CoA. """
    playbook = Playbook(coa).generate()
    return playbook


@app.post("/getCoAResults")
async def getCoAResults(request_timestamp: int):
    """ Get previous CoA results from its timestamp, and extract the associated graph. """
    file_name = f'{rp_config.PREFIX_FILENAME}{request_timestamp}{rp_config.SUFFIX_FILENAME}'
    data = readJsonFile(file_name=file_name)
    return data



@app.get("/getAllSimulationsIds")
async def getAllSimulationsIds(response: Response):
    """ Get all the simulations IDs from the ADG model. """
    try:
        all_simulations_ids = AdgCommunicator().get_all_simulations_ids()
    except AdgCommunicatorError as e:
        response.status_code = status.HTTP_403_FORBIDDEN
        error_message = {"error":e.__repr__(), "cause":e.__cause__}
        print(error_message)
        return error_message
    except ValueError as e:
        response.status_code = status.HTTP_403_FORBIDDEN
        error_message = {"error":e.__repr__(), "context":e.__context__}
        print(error_message)
        return error_message
    return all_simulations_ids


def getRp_gui_address(request):
    if rp_config.RP_GUI_ADDRESS:
        rp_gui_address = rp_config.RP_GUI_ADDRESS
    else:
        rp_gui_address = request.client.host
    return rp_gui_address


def readJsonFile(file_name):
    f = open(file_name)
    data = json.load(f)
    f.close()
    return data

if __name__ == "__main__":
    uvicorn.run(app, port=8000, log_level="info")
