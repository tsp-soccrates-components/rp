import time

from app.rp_config import OPERATOR_EMAIL
from app.workFlowDefense import HUMAN_TARGET_TAG, OPENC2_PROXY_TARGET_TAG, WorkFlowDefense

# Constants
PLAYBOOK_CREATOR = "Soccrates_Response_Planner"
PLAYBOOK_BASE = {
    "type": "playbook",
    "spec_version": "1.0",
    "created_by":PLAYBOOK_CREATOR,
    "description": "This playbook will apply the CoA in the system",
    "playbook_types": ["prevention"],
    "features": {"parallel_processing": True},
}
ID_BASE = "soccrates-playbook-"
NAME_BASE = "Soccrates CoA Playbook "
HUMAN_TARGET = {"type":"individual", "email":{"work": OPERATOR_EMAIL}}
OPENC2_PROXY_TARGET = {
    "type": "security-infrastructure-category",
    "category": ["firewall", "endpoint"],
    }
WORFLOW_DEFENSE_TYPE = 'single'


class Playbook():
    def __init__(self, coa):
        self.timestamp = time.time_ns()
        self.id = f'{ID_BASE}{self.timestamp}'
        self.name = f'{NAME_BASE}{self.timestamp}'
        self.workflow =  WorflowPlaybook(coa, self.id)

    def generate(self):
        content = dict(PLAYBOOK_BASE)
        content["created"] = time.ctime(self.timestamp/10**9)
        content["id"] = self.id
        content["name"] = self.name
        content["workflow_start"] = self.workflow.start_id
        content["workflow"] = self.workflow.content
        content["targets"] = self.get_targets()
        return content

    def get_targets(self):
        targets = {}
        target_ids = set()
        for workflowStep in self.workflow.content.values():
            print(workflowStep)
            if workflowStep['type'] == WORFLOW_DEFENSE_TYPE:
                ws_target_ids = workflowStep["target_ids"]
                target_ids.update(ws_target_ids)
        if HUMAN_TARGET_TAG in target_ids:
            targets[HUMAN_TARGET_TAG] = HUMAN_TARGET
        if OPENC2_PROXY_TARGET_TAG in target_ids:
            targets[OPENC2_PROXY_TARGET_TAG] = OPENC2_PROXY_TARGET
        return targets



class WorflowPlaybook():
    def __init__(self, coa, playbook_id):
        self.content = {}
        self.playbook_id = playbook_id
        self.containment = coa["containment"]
        self.defenses = coa["defenses"]
        self.start_id = f'start--{self.playbook_id}'
        self.end_id = f'end--{self.playbook_id}'
        wf_start, parallel_id = self.gen_wf_start()
        self.content[self.start_id] = wf_start
        wf_parallel, defenses_ids = self.gen_wf_parallel()
        self.content[parallel_id] = wf_parallel
        self.add_wf_defenses(defenses_ids)
        self.content[self.end_id] = {"type": "end"}


    def gen_wf_start(self):
        """ Add a starting point to the playbook, pointing to a parallel task."""
        parallel_id = f'parallel--{self.playbook_id}'
        workflow_start = {"type":"start", "on_completion":parallel_id}
        return workflow_start, parallel_id


    def gen_wf_parallel(self):
        """ Add a parallel task pointing to all the defenses steps in parallel."""
        type = "parallel"
        name = "Apply defenses of the CoA"
        defenses_ids = []
        for i in range(len(self.defenses)):
            defense_wf_id = f'defense-{i+1}--{self.playbook_id}'
            defenses_ids.append(defense_wf_id)
        wf_parallel = {"type":type,"name":name,"next_steps":defenses_ids}
        return wf_parallel, defenses_ids


    def add_wf_defenses(self, defenses_ids):
        """ Add all the CoA defenses to the steps of workflow."""
        for index, defense in enumerate(self.defenses):
            wf_defense_id = defenses_ids[index]
            wf_defense = WorkFlowDefense(defense, self.end_id).asdict()
            self.content[wf_defense_id] = wf_defense
