import requests

from app.adgCommunicator import AdgCommunicator
from app.coaSaver import CoaSaver
from app.containmentDefense import ContainmentDefense, DefenseName
from app.playbook import Playbook
import app.rp_config as rp_config
from app.siemLabel import SiemLabel


# Constants
RESPONSE_FIELDS = ['RORI', 'business_impact', 'containment', 'playbook', 'defenses', 'monetary_cost', 'defenses_hosts']
BUSINESS_IMPACT_VALUE_ERROR = -1


class CoasEvaluator():
    def __init__(self, containment=False, siem_label='', aiv=12345, aar=1,
                 initial_ids={}):
        self.containment = containment
        self.siem_label = siem_label
        self.aiv = aiv
        self.aar = aar
        self.initial_ids = initial_ids
        self.evaluated_coas = []

    def eval(self, coas):
        self.coas = coas
        self.evaluated_coas = []
        self.coa_evaluator = self.get_coa_evaluator()
        self.ale = self.coa_evaluator.get_ale()
        for coa in coas:
            evaluated_coa = self.coa_evaluator.eval(coa.dict())
            self.evaluated_coas.append(evaluated_coa)
        self.sort_evaluated_coas()
        coaSaver = CoaSaver(coas=self.evaluated_coas,
                            containment=self.containment,
                            ale=self.ale,
                            aiv=self.aiv)
        coaSaver.save_to_json()
        report_url = coaSaver.get_report_url()
        ranked_coas = self.generate_ranked_coas()
        return {'ranked_coas': ranked_coas, 'report_url': report_url}

    def get_coa_evaluator(self):
        if not(self.containment):
            if self.initial_ids:
                coa_evaluator = ClassicCoaEvaluator(siem_label=self.siem_label,
                                                    aiv=self.aiv,
                                                    aar=self.aar,
                                                    initial_ids=self.initial_ids)
        else:
            coa_evaluator = ContainmentCoaEvaluator()
        return coa_evaluator

    def sort_evaluated_coas(self):
        """Sort the evaluated_coa list by RORI or by business_impact."""
        if self.containment:
            sortingKey="business_impact"
        else:
            sortingKey="RORI"
        self.evaluated_coas.sort(key=lambda coa:coa[sortingKey], reverse=True)

    def generate_ranked_coas(self):
        ranked_coas = []
        for coa in self.evaluated_coas:
            ranked_coa = {key: coa[key] for key in RESPONSE_FIELDS}
            ranked_coas.append(ranked_coa)
        return ranked_coas


class BaseCoasEvaluator():
    def __init__(self):
        super().__init__()

    def generate_evaluated_coa(self, coa, business_impact=None, rori=None,
                               monetary_cost=None, containment=False):
        evaluated_coa = dict(coa)
        evaluated_coa['business_impact'] = business_impact
        evaluated_coa['RORI'] = rori
        evaluated_coa['monetary_cost'] = monetary_cost
        evaluated_coa["containment"] = containment
        evaluated_coa["defenses_hosts"] = self.get_coa_defenses_hosts(coa)
        evaluated_coa["playbook"] = Playbook(coa).generate()
        return evaluated_coa

    def get_coa_defenses_hosts(self, coa):
        coa_defenses_hosts = set()
        coa_defenses = coa["defenses"]
        for defense in coa_defenses:
            defense['ref'] = self.get_defense_ref(defense)
            if type(defense["ref"]) == list:
                coa_defenses_hosts.update(defense["ref"])
            else:
                coa_defenses_hosts.update([defense["ref"]])
        return list(coa_defenses_hosts)

    def get_defense_ref(self, defense):
        if 'ref' in defense:
            return defense['ref']
        elif ('hosts' in defense):
            return defense["hosts"]
        elif ('flows' in defense):
            def_ref = set()
            for flow in defense["flows"]:
                def_ref.update([flow["src"]])
                def_ref.update([flow["dst"]])
            return list(def_ref)
        return []


class ContainmentCoaEvaluator(BaseCoasEvaluator):
    def __init__(self):
        super().__init__()

    def eval(self, coa):
        coa_defenses = coa["defenses"]
        hosts_to_isolate = self.get_hosts_to_isolate(coa_defenses)
        business_impact = self.get_business_impact_from_BIA(hosts_to_isolate)
        containment = True
        evaluated_coa = self.generate_evaluated_coa(coa=coa,
                                                    business_impact=business_impact,
                                                    containment=containment)
        return evaluated_coa

    def get_hosts_to_isolate(self, coa_defenses):
        hosts_to_isolate = set()
        for defense in coa_defenses:
            if ContainmentDefense.is_host_isolation(defense['defenseName']):
                hosts_to_isolate.update(defense['hosts'])
        return list(hosts_to_isolate)

    def get_business_impact_from_BIA(self, hosts_to_isolate):
        """ Get, from the BIA API, the Business impact evaluation of the containment CoA."""
        if hosts_to_isolate:
            business_impact = self.get_host_isolation_impact_from_BIA(hosts_to_isolate)
            return business_impact
        return None

    def get_host_isolation_impact_from_BIA(self, hosts_to_isolate):
        bia_address = f'{rp_config.BIA_BACKEND_SERVICE}'
        service_bia_coa = "computeBI_Containment_CoA"
        defenseName = DefenseName.HOST_ISOLATION.value
        bia_request_url = f'{bia_address}/{service_bia_coa}?defenseName={defenseName}'
        request_payload = hosts_to_isolate
        try:
            bia_api_request = requests.post(bia_request_url, json = request_payload)
            if bia_api_request.status_code == 200:
                coa_impact = float(bia_api_request.text)
                return coa_impact
            else:
                print(f"Bia request error response: {bia_api_request.text}")
        except Exception as e:
            print(e)
            return BUSINESS_IMPACT_VALUE_ERROR
        return BUSINESS_IMPACT_VALUE_ERROR

    def get_ale(self):
        "Can't evaluate a ALE value for Containment Coas"
        return 0


class ClassicCoaEvaluator(BaseCoasEvaluator):
    def __init__(self, siem_label, aiv, aar, initial_ids):
        super().__init__()
        self.aiv = aiv
        self.attack_dangerousness = self.get_attack_dangerousness(siem_label, aar)
        self.adgCommunicator = AdgCommunicator()
        initial_sim_result = self.get_sim_result_from_ids(initial_ids)
        self.initial_risk = initial_sim_result["risk"]
        self.initial_maxrisk = initial_sim_result["maxrisk"]
        self.threat_score = self.get_threat_score()
        self.ale = self.get_ale()

    def get_attack_dangerousness(self, siem_label, aar):
        exposure_factor = SiemLabel(siem_label).get_exposure_factor()
        attack_dangerousness = aar * exposure_factor
        return attack_dangerousness

    def get_sim_result_from_ids(self, sim_ids):
        pid = sim_ids["pid"]
        tid = sim_ids["tid"]
        simid = sim_ids["simid"]
        sim_result = self.adgCommunicator.get_sim_result(pid=pid, tid=tid, simid=simid)
        return sim_result

    def get_threat_score(self):
        """Theat score is the ratio ALE/AIV. """
        threat_score = self.attack_dangerousness * self.initial_risk / self.initial_maxrisk
        return threat_score

    def get_ale(self):
        ale = self.threat_score * self.aiv
        return ale

    def eval(self, coa):
        rm = self.compute_rm(coa)
        arc = self.compute_arc(coa)
        rori = self.compute_rori(rm, arc)
        containment = False
        evaluated_coa = self.generate_evaluated_coa(coa=coa,
                                                    rori=rori,
                                                    monetary_cost=arc,
                                                    containment=containment)
        return evaluated_coa

    def compute_rm(self, coa):
        coa_ids = coa["coa_ids"]
        coa_sim_result = self.get_sim_result_from_ids(coa_ids)
        coa_risk = coa_sim_result["risk"]
        coa_maxrisk = coa_sim_result["maxrisk"]
        if coa_maxrisk != self.initial_maxrisk:
            # Level coa_risk to initial_risk
            coa_risk *= self.initial_maxrisk / coa_maxrisk
        rm = (self.initial_risk - coa_risk) / self.initial_risk
        return rm

    def compute_arc(self, coa):
        """Compute the Annual Response Cost by adding the monetary costs of the CoA."""
        arc = sum(coa["monetary_cost"].values())
        return arc

    def compute_rori(self, rm, arc):
        rori = (rm * self.ale - arc) / (arc + self.aiv) * 100
        return rori
