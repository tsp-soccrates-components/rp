# RP - Response Planner

The Response Planner is a component of the SOCCRATES plateform that is in charged of evaluating Courses of Action (CoAs). It is using the RORI metric to compare and ranks CoAS. In order to evaluate parameters of the RORI metric, the RP fetch some information on the CoA risk mitigation to the ADG SOCCRATES component.

The RP is also in charged of generating Containment CoAs. Containment CoAs are short term countermeasure to isolate hosts or to filter network traffic. 

To evaluate Containment CoA, the RP communicate with the BIA SOCCRATES component, to assess the business impact of the countermeasure.

Eventually, after evaluating CoAs, the RP generates CACAO playbooks for each CoA, to help for their deployment.

## Directories

- backend:
Contains the _Python_ API of the Response Planner, made with _fastapi_ and _uvicorn_ librairies.

- frontend:
Contains the _JavaScript_ Graphical User Interface of the Response Planner,
based on _Tabulator_.

# Addresses and Ports Configuration

## RP API address:

If the RP api access address for the client is changed, the following variables
should be updated:

frontend/gui/rp_table_visu.js:
- _rpApiAddress_ (line 2)

For Cortex Analysers :

If the RP api address or port are changed, the following variables in the Cortex
Analyser should be updated:

SOCCRATES_RP_COA_RANKING configurationItems: _responsePlannerApi_url_

SOCCRATES_RP_CONTAINMENT_COA_GENERATOR configurationItems: _responsePlannerApi_url_

## RP GUI port:

If the RP GUI address or port are changed, the following variables should be updated:

backend/app/rp_config.py:
- _HOST_IP_OR_DOMAIN_ (line 5)
- _RP_GUI_ADDRESS_ (line 6)

## BIA Addresses modification

If access to the bia is changed, or the service name of
the bia has changed, the following variables should be updated:

backend/app/rp_config.py :
- _BIA_BACKEND_SERVICE_: the address to access bia backend service. (line 9)

## RORI parameters
If you want to change the RORI parameters, the following variables can be
updated:

backend/app/rp_config.py:
- _AAR_: Annual Attack Rate (line 18)
- _AIV_: Annual Infrastructure Value (line 16)


# Docker image building and deployment

## Build docker Image

In the backend repository:

sudo docker build -t rp-backend .

In the frontend repository:

sudo docker build -t rp .


## Test the docker deployment

### Run docker image

sudo docker run -it rp

### Get the docker Ip address

sudo docker ps

sudo docker inspect $containerID

## Test the API and GUI

RP api: _http://$containerIP:8000/docs_

RP graphical user interface: _http://$containerIP:80_

### Test the RP API

The RP backend should be accessible at _https://rp-backend.soccrates.xyz_
It should also be accessible at _http://$containerPrivateIP:8000_ or _http://$publicIP:8000_

Go to _https://rp-backend.soccrates.xyz/docs_ in order to test the methods of the API.
- Click on the method you want to test, and click on **Try it out** button.

_generateContainmentCoas_ and _coa_ranking_ are the two RP API methods that should be tested.

#### generateContainmentCoas

##### Input example
- type: containment or stop_exfiltration
- siem_label: TA0001, TA0008, TA0010 or leave it empty
Do not add quote or double quote in _type_ and _siem_label_ input box.

Request body:
```
{
  "sourceIPs": [
    "192.168.0.3",
    "192.168.0.10"
  ],
  "destinationIPs": [
    "123.45.67.89",
    "123.98.76.54"
  ]
}
```
If possible replace the sourceIPs addresses with existing one (listed in the BIA model/IMC model).

##### Expected output
Code 200
```
{
  "containment": true,
  "coas": [
    {
      "defenses": [
        {
          "defenseName": "Host isolation",
          "defenseInfo": "Isolate the host from the network by using Network Segmentation",
          "hosts": [
            "192.168.0.3",
            "192.168.0.10"
          ],
          "mitreRef": "M1030"
        }
      ]
    }
  ]
}
```
#### coa_ranking

##### Non containment CoA

**Input Example**
- siem_label: TA0001, TA0008, TA0010 or leave it empty
- containment: false
- Request body: Leave it as it is by default (with two example CoAs).

**Expected output**

Code 200
```
{
  "ranked_coas": [
    {
      "RORI": 7.477219757603345,
      "business_impact": null,
      "containment": false,
      "playbook": {
        "type": "playbook",
[...]
      "monetary_cost": 2906,
      "defenses_hosts": [
        "112255",
        "112244"
      ]
    }
  ],
  "report_url": "http://10.13.7.6:8001/?request=1649685695623020816"
}
```

##### Containment CoA


**Input Example**
- siem_label: TA0001, TA0008, TA0010 or leave it empty
- containment: true
- Request body: Take the output payload from a generateContainmentCoas request.
```
{
  "containment": true,
  "coas": [
    {
      "defenses": [
        {
          "defenseName": "Host isolation",
          "defenseInfo": "Isolate the host from the network by using Network Segmentation",
          "hosts": [
            "192.168.0.3",
            "192.168.0.10"
          ],
          "mitreRef": "M1030"
        }
      ]
    }
  ]
}
```

**Expected output**

Code 200
```
{
  "ranked_coas": [
    {
      "RORI": null,
      "business_impact": 0,
      "containment": true,
      "playbook": {
        "type": "playbook",
        "spec_version": "1.0",
[...]
      "monetary_cost": null,
      "defenses_hosts": [
        "192.168.0.3",
        "192.168.0.10"
      ]
    }
  ],
  "report_url": "http://10.13.7.6:8001/?request=1649685929353688548"
}
```

##### report_url

The report_url in the Response body should look like "http://10.13.7.6:8001/?request=1649685929353688548", but in order to access to the RP GUI you would have to **change the IP address** of the URL and maybe the port, with the corresponding one. (The address used to connect to the RP).


### Test the RP frontend

The RP frontend should be accessible at http://rp.soccrates.xyz/.
It is also accessible at _http://$containerPrivateIP:8000_ or _http://$publicIP:8001_.

By default, the GUI will display the result of two evaluated COAs.

After a request to the RP API **CoA Ranking** service, you can obtain
an url to observe the result of the request, such as
 _https://rp.soccrates.xyz/?request=1649334690695447283_.

 You may have to change the address of the url '10.13.7.26' with the correct IP
 used to access to the RP.

 #### Invalid url argument request

 If an invalid timestamp is given as request argument in the url, an error message
  will be displayed instead of the frontend.

#### Table columns filling

 If Containment CoAs were evaluated:
- the Business Impact column should be filled
- the RORI and Annual Cost columns should be empty
- ALE should be zero


 If Non Containment CoAs were evaluated:
- the Business Impact column should be empty
- the RORI and Annual Cost columns should be filled

#### Business Impact Value

- Empty when displaying CoAs that are not of the containment type.
- Value between 0 and 1 when displaying Containment CoAs.
- Value -1, when the RP didn't successfully reach out the BIA API.
- Value 0, If CoA doesn't have any impact or if IP Addresses used in the CoA defenses (Host Isolation) are **not included** in the BIA model.


#### GUI Features Tests:

- Show details button:

 This button should developp the CoA table with another line for each CoA contain a table describing its defense.

- Select CoAs:

It should be possible to select one or several CoAs inside the table by clicking on its line. When selected the COA line background will turn blue instead of grey.

You can also select all CoAs or deselect all CoAs by clicking on the according button.

- Get Playbook button:

After having select one or several CoAs, clicking on the Get Playbook button should open a new tab for every CoAs selected.
 This new tab should contain a CACAO Playbook, than can be seen as a json file, with a description of every defenses of the CoA.

This button will send a request to the RP API, using the same address as the one you are using in your web browser, but using the port "RP_API_port" to reach for the API. The RP API should be accessible on the same address of the RP GUI, but on a different port.

If on clicking, nothing happen, check that you have selected at least one CoA (Blue background on its line).

If nothing happen, on the console of your web browser you should have a "Cross-Origin Request Blocked" error. This may be due to a configuration error of the RP_API_port inside the GUI. This parameter can be changed dynamically in the GUI, with the RP API Port text box.




## Swarm deployment

Swarm deployment to be fixed:


```bash
# replace soccrates.xyz with the IP of your swarm manager or your domain name
HOST_IP_OR_DOMAIN=soccrates.xyz make -B rp
```


